#coding=utf-8
"""
Vicente Illanes, CC3501, Tarea 3

Solución ecuación de Laplace para el potencial eléctrico producido por un volcán, sobre un dominio no rectangular con condiciones de borde 
tipo Dirichlet y Neumann
"""

import numpy as np
import matplotlib.pyplot as mpl
from scipy.sparse import csc_matrix
from scipy.sparse.linalg import spsolve

import warnings
warnings.filterwarnings("ignore")

import sys

def getK(i,j):
    return j * nh + i

def getIJ(k):
    i = k % nh
    j = k // nh
    return (i, j)

if __name__ == "__main__":
    # Ajustes Problema, se dividen distancia por un factor de 10.000
    H = 1       #altura
    W = 2       #ancho
    E = float(sys.argv[2])   #potencial en el crater, condición de borde Neumann
    h = float(sys.argv[1])  #espaciado

    # Condiciones de borde Dirichlet:
    TOP = 0
    BOTTOM = 0
    LEFT = 0
    RIGHT = 0

# Number of unknowns
# left, bottom and top sides are known (Dirichlet condition)
# right side is unknown (Neumann condition)
    nh = int(W / h) - 1
    nv = int(H / h) - 1

# In this case, the domain is just a rectangle
    N = nh * nv

# We define a function to convert the indices from i,j to k and viceversa
# i,j indexes the discrete domain in 2D.
# k parametrize those i,j, this way we can tidy the unknowns
# in a column vector and use the standard algebra



# In this matrix we will write all the coefficients of the unknowns
    A = csc_matrix((N, N))

# In this vector we will write all the right side of the equations
    b = np.zeros((N,))

# Note: To write an equation is equivalent to write a row in the matrix system

# We iterate over each point inside the domain
# Each point has an equation associated
# The equation is different depending on the point location inside the domain


    lado_izq = int(0.94/h)
    base = int(0.12/h)
    volcan = int(0.25/h)
    ladera = int(0.1/h)
    lado_der = int(0.84/h)
    suelo = int(2/h)

    for i in range(lado_izq, lado_izq + base):
        for j in range(0, volcan):
            k = getK(i,j)
            A[k, k] = 1

    def paint(A, xi, yi, xf, yf):
        for i in range(xi, xf):
            for j in range (yi, yf):
                k = getK(i,j)
                A[k,k]=1

    salto = int(volcan/ladera)
    count = 0
    for j in range(0,volcan):
        if(j != 0 and j%salto == 0): count = count +1
        paint(A, lado_izq + base, j , lado_izq + base + ladera - count, j+1)
        
    for i in range(0, nh):
        for j in range(0, nv):

            # We will write the equation associated with row k
            k = getK(i,j)

            # We obtain indices of the other coefficients
            k_up = getK(i, j+1)
            k_down = getK(i, j-1)
            k_left = getK(i-1, j)
            k_right = getK(i+1, j)

            # Depending on the location of the point, the equation is different
            # Interior
            if 1 <= i and i <= nh - 2 and 1 <= j and j <= nv - 2:
                if A[k,k] == 1:
                    continue
                
                elif i >= lado_izq and i < lado_izq +base and j == volcan:
                    A[k, k_up] = 2
                    A[k, k_down] = 0
                    A[k, k_left] = 1
                    A[k, k_right] = 1
                    A[k, k] = -4
                    b[k] = -2 * h * E
                else:
                    A[k, k_up] = 1
                    A[k, k_down] = 1
                    A[k, k_left] = 1
                    A[k, k_right] = 1
                    A[k, k] = -4
                    b[k] = 0
            
            # left side
            elif i == 0 and 1 <= j and j <= nv - 2:
                A[k, k_up] = 1
                A[k, k_down] = 1
                A[k, k_right] = 1
                A[k, k] = -4
                b[k] = -LEFT
            
            # right side
            elif i == nh - 1 and 1 <= j and j <= nv - 2:
                A[k, k_up] = 1
                A[k, k_down] = 1
                A[k, k_left] = 1
                A[k, k] = -4
                b[k] = -RIGHT
            
            # bottom side
            elif 1 <= i and i <= nh - 2 and j == 0:
                if A[k,k] == 1:
                    continue
                else:
                    A[k, k_up] = 1
                    A[k, k_left] = 1
                    A[k, k_right] = 1
                    A[k, k] = -4
                    b[k] = -BOTTOM
            
            # top side
            elif 1 <= i and i <= nh - 2 and j == nv - 1:
                A[k, k_down] = 1
                A[k, k_left] = 1
                A[k, k_right] = 1
                A[k, k] = -4
                b[k] = -TOP

            # corner lower left
            elif (i, j) == (0, 0):
                A[k, k_up] = 1
                A[k, k_right] = 1
                A[k, k] = -4
                b[k] = -BOTTOM - LEFT

            # corner lower right
            elif (i, j) == (nh - 1, 0):
                A[k, k_up] = 1
                A[k, k_left] = 1
                A[k, k] = -4
                b[k] = -BOTTOM - RIGHT

            # corner upper left
            elif (i, j) == (0, nv - 1):
                A[k, k_down] = 1
                A[k, k_right] = 1
                A[k, k] = -4
                b[k] = -TOP - LEFT

            # corner upper right
            elif (i, j) == (nh - 1, nv - 1):
                A[k, k_down] = 1
                A[k, k_left] = 1
                A[k, k] = -4
                b[k] = -TOP - RIGHT

            else:
                print("Point (" + str(i) + ", " + str(j) + ") missed!")
                print("Associated point index is " + str(k))
                raise Exception()


    # A quick view of a sparse matrix
    #mpl.spy(A)

    # Solving our system
    x = spsolve(A, b)

    # Now we return our solution to the 2d discrete domain
    # In this matrix we will store the solution in the 2d domain
    u = np.zeros((nh,nv))

    for k in range(0, N):
        i,j = getIJ(k)
        u[i,j] = x[k]

    # Adding the borders, as they have known values
    ub = np.zeros((nh + 1, nv + 1))
    ub[1:nh + 1, 0:nv] = u[:,:]

    # Dirichlet boundary condition
    # top 
    ub[0:nh + 2, nv] = TOP
    # bottom izq
    ub[0:lado_izq+1, 0] = BOTTOM
    # bottom der
    ub[lado_izq+base+ladera+1:nh+2, 0] = BOTTOM
    # left
    ub[0, 1:nv ] = LEFT
    # right
    ub[nh, 1:nv] = RIGHT

    #Para la diferencia de potencial
    #print(ub[int(1/h), int(0.5/h)] - ub[int(1/h), int(0.75/h)])

    # Se guarda solución en archivo solution.npy
    np.save(sys.argv[3], np.array([ub, h, E]))