from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np




ubh= np.load("solution.npy")

ub = ubh[0]
h = ubh[1]
E = ubh[2]

W = 2
H = 1

nh = int(W / h) - 1
nv = int(H / h) - 1


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')



ax.set_xlabel('Ancho [m]')
ax.set_ylabel('Altura [m]')
ax.set_zlabel('Potencial Eléctrico [V]')
ax.set_title('Solución ecuación de Laplace para el' + '\n' +'potencial eléctrico como Superficie  ' + '\n' + 'con h =' + str(h) + 'y E =' + str(E))

# Make data.
X = np.arange(0, nh+1, 1)
Y = np.arange(0, nv+1 , 1)
X, Y = np.meshgrid(X, Y)
Z = ub.T



i = 0
jump = int(20000/(len(ax.get_xticklabels())-1))
xlabel = []
while i < len(ax.get_xticklabels()):
    xlabel = xlabel + [i*jump]
    i = i +1

j = 0
jump = int(10000/(len(ax.get_yticklabels())-1))
ylabel = []
while j < len(ax.get_yticklabels()):
    ylabel = ylabel + [j*jump]
    j = j +1


# Plot the surface.
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

i_dos= 0
jump = int(20000/(len(ax.get_xticklabels())-1))
xlabel_dos = []
while i_dos < len(ax.get_xticklabels()):
    xlabel_dos = xlabel_dos + [i_dos*jump]
    i_dos = i_dos +1

j_dos = 0
jump = int(10000/(len(ax.get_yticklabels())-1))
ylabel_dos = []
while j_dos < len(ax.get_yticklabels()):
    ylabel_dos = ylabel_dos + [j_dos*jump]
    j_dos = j_dos +1


if(20000 - xlabel[i-1] >= 0 and 20000 - xlabel[i-1] < 20000 - xlabel_dos[i_dos-1]):
    ax.set_xticklabels(xlabel)
    ax.set_yticklabels(ylabel)
else:
    ax.set_xticklabels(xlabel_dos)
    ax.set_yticklabels(ylabel_dos)

                       


plt.show()