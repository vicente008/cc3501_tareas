from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np


ubh= np.load("solution.npy")

ub = ubh[0]
h = ubh[1]
E = ubh[2]

W = 2
H = 1

nh = int(W / h) - 1
nv = int(H / h) - 1


fig = plt.figure()
ax = fig.add_subplot(111)

# Make data.
X = np.arange(0, nh+1, 1)
Y = np.arange(0, nv+1 , 1)
X, Y = np.meshgrid(X, Y)
Z = ub.T

plt.contour(X, Y, Z)
ax.set_xlabel('Ancho [m]')
ax.set_ylabel('Altura [m]')
ax.set_title('Solución ecuación de Laplace para el' + '\n' +'potencial eléctrico con curvas de nivel'+ '\n' + 'con h =' + str(h)+ 'y E =' + str(E))

i = 0
jump = int(20000/(len(ax.get_xticklabels())-1))
xlabel = []
while i < len(ax.get_xticklabels()):
    xlabel = xlabel + [i*jump]
    i = i +1

j = 0
jump = int(10000/(len(ax.get_yticklabels())-1))
ylabel = []
while j < len(ax.get_yticklabels()):
    ylabel = ylabel + [j*jump]
    j = j +1


ax.set_xticklabels(xlabel)
ax.set_yticklabels(ylabel)

plt.show()