#coding=utf-8
"""
Vicente Illanes, CC3501, Tarea 3

Gráfico solución de la ecuación de Laplace para el potencial en archivo solution.npy
"""
import numpy as np
import matplotlib.pyplot as mpl
import matplotlib.pylab as pl


ubh= np.load("solution.npy")

ub = ubh[0]
h = ubh[1]
E = ubh[2]

mpl.rcParams['axes.facecolor'] = '000000'
fig = pl.figure()
ax = fig.add_subplot(111)
pcm = ax.pcolormesh(ub.T, cmap='RdBu_r')
fig.colorbar(pcm)
ax.set_xlabel('Ancho [m]')
ax.set_ylabel('Altura [m]')
ax.set_title('Solución ecuación de Laplace para el' + '\n' +'potencial eléctrico'+ '\n' + 'con h =' + str(h)+ 'y E =' + str(E))

i = 0
jump = int(20000/(len(ax.get_xticklabels())-1))
xlabel = []
while i < len(ax.get_xticklabels()):
    xlabel = xlabel + [i*jump]
    i = i +1

j = 0
jump = int(10000/(len(ax.get_yticklabels())-1))
ylabel = []
while j < len(ax.get_yticklabels()):
    ylabel = ylabel + [j*jump]
    j = j +1


ax.set_xticklabels(xlabel)
ax.set_yticklabels(ylabel)

mpl.show()