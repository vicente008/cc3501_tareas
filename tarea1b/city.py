# coding=utf-8
"""
Vicente Illanes, CC3501, 2019-1
Escena del bosque
"""

import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import transformations as tr
import scene_graph as sg
import sys
import math

# We will use 32 bits data, so an integer has 4 bytes
# 1 byte = 8 bits
INT_BYTES = 4


# A class to store the application control
class Controller:
    x = 0.0
    y = 0.0
    zoom = 1.0
    fillPolygon = True
    useNight = False


# we will use the global controller as communication with the callback function
controller = Controller()

def on_key(window, key, scancode, action, mods):
    
    global controller

    if action == glfw.REPEAT or action == glfw.PRESS:

        if key == glfw.KEY_LEFT:
            controller.x -= 0.1
        elif key == glfw.KEY_RIGHT:
            controller.x += 0.1
        elif key == glfw.KEY_UP:
            controller.y += 0.1
        elif key == glfw.KEY_DOWN:
            controller.y -= 0.1
        elif key == glfw.KEY_Q and controller.zoom > 0.2:
            controller.zoom -= 0.1

        elif key == glfw.KEY_W and controller.zoom < 1:
            controller.zoom += 0.1

        elif action != glfw.PRESS:
            return

        elif key == glfw.KEY_ESCAPE:
            sys.exit()
    
        elif key == glfw.KEY_SPACE:
            controller.useNight = not controller.useNight

        else:
            print('Unknown key')

def drawShape(shaderProgram, shape, transform):
    # Binding the proper buffers
    glBindVertexArray(shape.vao)
    glBindBuffer(GL_ARRAY_BUFFER, shape.vbo)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ebo)

    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "transform"), 1, GL_FALSE, transform)

    # Setting up the location of the attributes position and color from the VBO
    # A vertex attribute has 3 integers for the position (each is 4 bytes),
    # and 3 numbers to represent the color (each is 4 bytes),
    # Henceforth, we have 3*4 + 3*4 = 24 bytes
    position = glGetAttribLocation(shaderProgram, "position")
    glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(0))
    glEnableVertexAttribArray(position)

    color = glGetAttribLocation(shaderProgram, "color")
    glVertexAttribPointer(color, 3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(12))
    glEnableVertexAttribArray(color)

    # Render the active element buffer with the active shader program
    glDrawElements(GL_TRIANGLES, shape.size, GL_UNSIGNED_INT, None)
    
def createQuad(r, g, b):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape    
    vertexData = np.array([
    #   positions        colors
        -0.5, -0.5, 0.0,  r, g, b,
         0.5, -0.5, 0.0,  r, g, b,
         0.5,  0.5, 0.0,  r, g, b,
        -0.5,  0.5, 0.0,  r, g, b
    # It is important to use 32 bits data
        ], dtype = np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype= np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createTriangle(r,g,b):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining the location and colors of each vertex  of the shape
    vertexData = np.array(
    #     positions       colors
        [-0.3, -0.3, 0.0, r, g, b,
          0.3, -0.3, 0.0, r, g, b,
          0.0,  0.3, 0.0, r, g, b],
          dtype = np.float32) # It is important to use 32 bits data

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2], dtype= np.uint32)
        
    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createCircule(r,g,b):

    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    vertices = [0, 0, 0, r, g, b]
    N=100
    R =0.1
    for i in range(N):
        angle  = float(i) * 2.0 * np.pi / N
        vertices += [np.cos(angle)*R,np.sin(angle)*R, 0.0, r, g, b]
    # Defining the location and colors of each vertex  of the shape

    vertexData = np.array(vertices,
          dtype = np.float32) # It is important to use 32 bits data

    # Defining connections among vertices
    # We have a triangle every 3 indices specifiedin
    index = []
    for i  in range(1,N):
        index += [0,i, i+1]
    index += [0, N-1, 1]
    indices = np.array(
        index, dtype= np.uint32)
        
    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createSky():
    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
        #   positions  colors
        -1, -1, 0, 1, 1, 1,
        1, -1, 0, 1, 1, 1,
        1, 1, 0, 0.5, 0.5, 1,
        -1, 1, 0, 0.5, 0.5, 1
        # It is important to use 32 bits data
    ], dtype=np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype=np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def createGround():
    # Here the new shape will be stored
    gpuShape = sg.GPUShape()

    # Defining locations and colors for each vertex of the shape
    vertexData = np.array([
        #   positions    colors
        -1, -1, 0, 0.4, 0.4, 0.4,
        1, -1, 0, 0.4, 0.4, 0.4,
        1, -0.5, 0, 0.4, 0.4, 0.4,
        -1, -0.5, 0, 0.4, 0.4, 0.4,
        # It is important to use 32 bits data
    ], dtype=np.float32)

    # Defining connections among vertices
    # We have a triangle every 3 indices specified
    indices = np.array(
        [0, 1, 2,
         2, 3, 0], dtype=np.uint32)

    gpuShape.size = len(indices)

    # VAO, VBO and EBO and  for the shape
    gpuShape.vao = glGenVertexArrays(1)
    gpuShape.vbo = glGenBuffers(1)
    gpuShape.ebo = glGenBuffers(1)

    # Vertex data must be attached to a Vertex Buffer Object (VBO)
    glBindBuffer(GL_ARRAY_BUFFER, gpuShape.vbo)
    glBufferData(GL_ARRAY_BUFFER, len(vertexData) * INT_BYTES, vertexData, GL_STATIC_DRAW)

    # Connections among vertices are stored in the Elements Buffer Object (EBO)
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuShape.ebo)
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(indices) * INT_BYTES, indices, GL_STATIC_DRAW)

    return gpuShape

def crearNube():
    base = sg.SceneGraphNode("base")
    base.transform = tr.scale (0.6, 0.3, 0)
    base.childs += [createQuad(0.4,0.4,0.4)]

    nube = sg.SceneGraphNode("nube")
    nube.childs += [base]

    for i in range(4):
        circulo = sg.SceneGraphNode("circulo")
        circulo. transform = tr.translate(-0.3 + 0.2*i,0.15, 0)
        circulo. childs += [createCircule(0.4,0.4, 0.4)]
        nube.childs += [circulo]
    

    base_name = "circulo_"
    for i in range(4):
        circulo = sg.SceneGraphNode(base_name + str(i))
        circulo. transform = tr.translate(-0.3 + 0.2*i,-0.15, 0)
        circulo. childs += [createCircule(0.4,0.4, 0.4)]
        nube.childs += [circulo]
    
    circulo = sg.SceneGraphNode("circulop")
    circulo. transform = tr.translate(-0.35 ,0, 0)
    circulo. childs += [createCircule(0.4,0.4, 0.4)]
    nube.childs += [circulo]

    circulo = sg.SceneGraphNode("circulop")
    circulo. transform = tr.translate(0.35 ,0, 0)
    circulo. childs += [createCircule(0.4,0.4, 0.4)]
    nube.childs += [circulo]

    return nube

def crearCohete():

    cabeza = sg.SceneGraphNode("cabeza")
    cabeza.transform= tr.scale(0.5,0.5, 0)
    cabeza.childs+= [createTriangle(1,1,0)]

    cabeza_translate = sg.SceneGraphNode("cabeza_translate")
    cabeza_translate.transform= tr.translate(0,0.4, 0)
    cabeza_translate.childs+= [cabeza]

    base = sg.SceneGraphNode("base")
    base.transform = tr.scale (0.3, 0.6, 0)
    base.childs += [createQuad(1,0.1,0.1)]

    ala = sg.SceneGraphNode("ala")
    ala.transform = tr.scale (0.1, 0.3, 0)
    ala.childs += [createQuad(1,1,0.1)]

    ala_izq = sg.SceneGraphNode("ala_izq")
    ala_izq.transform = tr.translate(-0.15,-0.2,0)
    ala_izq.childs += [ala]

    ala_der = sg.SceneGraphNode("ala_der")
    ala_der.transform = tr.translate(0.15,-0.2,0)
    ala_der.childs += [ala]

    ala_cen = sg.SceneGraphNode("ala_cen")
    ala_cen.transform = tr.translate(0,-0.2,0)
    ala_cen.childs += [ala]

    cohete = sg.SceneGraphNode("cohete")
    cohete.childs += [cabeza_translate]
    cohete.childs += [base]
    cohete.childs += [ala_izq]
    cohete.childs += [ala_cen]
    cohete.childs += [ala_der]

    return cohete

#crea un edificio de altura h (con h pisos)
def crearEdificio(h):
    
    # Base
    base = sg.SceneGraphNode("base")
    base.transform = tr.scale(0.5,0.1*h,0)
    base.childs += [createQuad(0.3,0.3,0.3)]

    # Translacion Base
    baseTranslate = sg.SceneGraphNode("baseTranslate")
    baseTranslate.transform = tr.translate(0,-0.035,0)
    baseTranslate.childs += [base]
    
    #Ventanas
    ventanas = sg.SceneGraphNode("Ventanas")

    for j in range(h):
        piso = sg.SceneGraphNode("piso_" + str(j))
        piso.transform = tr.translate(0, (-0.1*h)/2.0 + j/10.0,0)
        ventanas.childs += [piso]

        for i in range(8):
            ventana = sg.SceneGraphNode("ventana_" + str(i))
            ventana.transform =  tr.matmul([tr.scale(0.03, 0.04, 1), tr.translate(-0.22 + i/16, 0, 0)])
            random_color = np.random.random_integers(4)
            if random_color ==2:
                ventana.childs += [createQuad(0.8,0.8,0.1)]
            else:
                ventana.childs += [createQuad(2,2,0.1)]
            piso.childs += [ventana]

    edificio = sg.SceneGraphNode("edificio") 
    edificio.childs += [baseTranslate]
    edificio.childs += [ventanas]

    return edificio


#crea un conjunto de edificios equispaciados
def crearEdificios(N):
    # Nodo al que se le agregaran los edificios
    edificios = sg.SceneGraphNode("edificios")

    # Cada edicio es creado y agregado como hijo de la raiz "edificios"
    baseName = "scaledEdificio_"
    for i in range(N):
        h = np.random.random_integers(5,15)
        altura_inicial = h*0.1
        scaledEdificio = sg.SceneGraphNode("scaledEdificio")
        scaledEdificio.transform = tr.uniformScale(1)
        scaledEdificio.childs += [crearEdificio(h)] 

        # Un edificio tiene una posición única en la escena por lo que su posición depende de su indice i
        newNode = sg.SceneGraphNode(baseName + str(i))
        newNode.transform = tr.translate((0.55) * i - (0.75-0.01), (0.1*h)/2 -0.5, 0)
        newNode.childs += [scaledEdificio]

        # Ahora el edificio es agregado a 'edificios' el grafo
        edificios.childs += [newNode]
    
    return edificios


if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 800
    height = 1000

    window = glfw.create_window(width, height, "City", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

     # Defining shaders for our pipeline
    vertex_shader = """
    #version 130
    in vec3 position;
    in vec3 color;

    out vec3 fragColor;

    uniform mat4 transform;

    void main()
    {
        fragColor = color;
        gl_Position = transform * vec4(position, 1.0f);
    }
    """

    fragment_shader = """
    #version 130

    in vec3 fragColor;
    out vec4 outColor;

    void main()
    {
        outColor = vec4(
fragColor.x * 0.5,
fragColor.y * 0.5,
clamp(fragColor.z + 0.2, 0, 1) * 0.8,
1.0f);
    }
    """
    fragment_shader_day = """
    #version 130

    in vec3 fragColor;
    out vec4 outColor;

    void main()
    {
        outColor = vec4(fragColor, 1.0f);
    }
    """


    # Assembling the shader program (pipeline) with both shaders
    shaderProgramNight = OpenGL.GL.shaders.compileProgram(
        OpenGL.GL.shaders.compileShader(vertex_shader, GL_VERTEX_SHADER),
        OpenGL.GL.shaders.compileShader(fragment_shader, GL_FRAGMENT_SHADER))

    shaderProgramDay = OpenGL.GL.shaders.compileProgram(
        OpenGL.GL.shaders.compileShader(vertex_shader, GL_VERTEX_SHADER),
        OpenGL.GL.shaders.compileShader(fragment_shader_day, GL_FRAGMENT_SHADER))
    

    # Setting up the clear screen color
    glClearColor(0.85, 0.85, 0.85, 1.0)

    # Creating shapes on GPU memory
    gpuSky = createSky()
    gpuGround = createGround()

    # Our shapes here are always fully painted
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
    N = int(sys.argv[1])
    edificios = crearEdificios(N)
    cohete = crearCohete()

    #suelo

    suelo = sg.SceneGraphNode("suelo")
    suelo.transform = tr.scale(N,0.5,0)
    suelo.childs += [createQuad(0.4, 0.4,0.4)]

    #sol
    sol = sg.SceneGraphNode("sol")
    sol.transform =  tr.translate(-0.7,1,0)
    sol.childs += [createCircule(1,1,0)]

    #Nubes
    nube = sg.SceneGraphNode("nube")
    nube.transform =  tr.uniformScale(0.3)
    nube.childs += [crearNube()]

    nubes = sg.SceneGraphNode("nubes")

    for i in range(N):
        nube_t = sg.SceneGraphNode("nube_translate_" + str(i))
        t = np.random.random_integers(1,10)
        nube_t.transform =  tr.translate(-1+i*0.5,  0.8,0)
        nube_t.childs += [nube]
        nubes.childs += [nube_t]

    #cohete
    cohete = sg.SceneGraphNode("cohete")
    cohete.childs += [crearCohete()]

    #escena solo con suelo

    escena_suelo = sg.SceneGraphNode("escena_suelo")
    escena_suelo.transform = tr.translate(0,-0.75, 0)
    escena_suelo.childs += [suelo]

    escena = sg.SceneGraphNode("escena")
    escena.childs += [escena_suelo]
    escena.childs += [sol]
    escena.childs += [edificios]
    escena.childs += [nubes]
    escena.childs += [cohete]



    while not glfw.window_should_close(window):

        
        # Using GLFW to check for input events
        glfw.poll_events()

        shaderProgram = shaderProgramDay
        # Telling OpenGL to use our shader program
        if controller.useNight:
            shaderProgram = shaderProgramNight
        # Telling OpenGL to use our shader program
        glUseProgram(shaderProgram)

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT)
    
    # Telling OpenGL to use our shader program
        glUseProgram(shaderProgram)

        # Drawing the Car
        drawShape(shaderProgram, gpuSky, tr.identity())

        # Assembling the shader program (pipeline) with both shaders
        shaderProgram = sg.basicShaderProgram()
        if controller.useNight:
            shaderProgram = sg.NightShaderProgram()
    
    # Telling OpenGL to use our shader program
        glUseProgram(shaderProgram)

        theta = -10 * glfw.get_time()
        cohete_move = sg.findNode(escena, "cohete")
        cohete_move.transform = tr.matmul([tr.uniformScale(0.2),tr.translate( 0.2 * np.sin(0.1 * theta) +0.7, 1, 0)]) 
        sg.drawSceneGraphNode(escena, shaderProgram, tr.matmul([tr.translate(controller.x, controller.y,0), tr.uniformScale(controller.zoom) ])   )     



        # Once the render is done, buffers are swapped, showing only the complete scene.
        glfw.swap_buffers(window)

    
    glfw.terminate()